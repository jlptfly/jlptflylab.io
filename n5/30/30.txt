Example #1
ドアを開ける前にノックぐらいしてください。
ドアをあけるまえにノックぐらいしてください。
doa o akeru mae ni nokku gurai shite kudasai.
Before opening the door, please at least knock first.
 --- 
Example #2
コンビニの前にじてんしゃがたくさんあります。
こんびにのまえにじいてんしゃがたくさんあります。
konbini no mae ni jitensha ga takusan arimasu.
There are many bicycles in front of the convenience store.
 --- 
Example #3
旅行の前に切符を買っておきます。
りょこうのまえにきっぷをかっておきます。
ryokou no mae ni kippu o katte okimasu.
I will buy the tickets before the trip.
 --- 
Example #4
ご飯の前に手を洗いましょう。
ごはんのまえにてをあらいましょう。
gohan no mae ni te o araimashou.
let's make sure to wash our hands before eating.
 --- 
Example #5
テストの前に一生懸命勉強しました。
てすとのまえにいっしょうけんめいべんきょうしました。
tesuto no mae ni isshokenmei benkyou shimashita.
I studied like crazy before the test.
 --- 
Example #6
大学を卒業する前に留学したいな。
だいがくをそつぎょうするまえにりゅうがくしたいな。
daigaku o sotsugyou suru mae ni ryuugaku shitai na.
I really want to study abroad sometime before I graduate university.
 --- 
Example #7
23時前に寝なければなりません。
23じまえにねなければなりません。
23 ji mae ni nenakereba narimasen.
I need to go to bed before 11pm.
 --- 
Example #8
寝る前に、歯みがきを忘すれないでね。
ねるまえに、はみがきをわすれないでね。
neru mae ni, hamigaki o wasurenai de ne.
Don't forget to brush your teeth before you go to bed.
 --- 
Example #9
試合の前にまだ一週間があるのでもっと練習しょう。
しあいのまえにまだいっしゅうかんがあるのでもっとれんしゅうしょう。
shiai no mae ni mada isshukan ga aru node motto renshuu shiyou.
We still have a week to go before the game so let's practice some more.
 --- 
Example #10
私たちの学校の前には美しい公園があります。
わたしたちのがっこうのまえにはうつくしいこうえんがあります。
watashi tachi no gakkou no mae ni wa utsukushii kouen ga arimasu.
There is a beautiful park in front of our school.