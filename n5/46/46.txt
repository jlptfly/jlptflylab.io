Example #1
何を飲みたいんですか。
なにをのみたいんですか。
nani o nomitai ndesu ka.
What would you like to drink?
 --- 
Example #2
お茶を飲みたいんです。
おちゃをのみたいんです。
ocha o nomitai ndesu.
I'd like to drink some tea.
 --- 
Example #3
眠くないんですか。
ねむくないんですか。
nemuku nai ndesu ka.
Aren't you sleepy?
 --- 
Example #4
気分が良くないんですよ。
きぶんがよくないんですよ。
kibun ga yokunai ndesu yo.
I'm not feeling well.
 --- 
Example #5
今からバイトに行くんだ。
いまからバイトにいくんだ。
ima kara baito ni iku nda.
I'm heading to my (part time) job now.
 --- 
Example #6
朝に弱いんだ。
あさによわいんだ。
asa ni yowai nda.
I'm not a morning person.
 --- 
Example #7
いつか日本に行くんだ！
いつかにほんにいくんだ！
itsuka nihon ni iku nda!
One day I will go to Japan!
 --- 
Example #8
買いたいんですがお金がないんだ。
かいたいんですがおかねがないんだ。
kaitai ndesu ga okane ga nai nda.
I want to buy it, but I have no money.