Example #1
私は寝るのが好きです。
わたしはねるのがすきです。
watashi wa neru no ga suki desu.
I like to sleep. / I like sleeping.
 --- 
Example #2
漫画を読むのが好きだ。
マンガをよむのがすきだ。
manga o yomu no ga suki da.
I like reading manga.
 --- 
Example #3
妹は料理をするのが好きです。
いもうとはりょうりをするのがすきです。
imouto wa ryouri o suru no ga suki desu.
My younger sister likes to cook.
 --- 
Example #4
君、運動するのが好きだね。
きみ、うんどうするのがすきだね。
kimi, undou suru no ga suki da ne.
You sure like working out don't you.
 --- 
Example #5
彼女は野球の試合を見るのが好きです。
かのじょはやきゅうのしあいをみるのがすきです。
kanojo wa yakyuu no shiai o miru no ga suki desu.
She likes watching baseball games.
 --- 
Example #6
私は何かを食べながら映画を観るのが好きです。
わたしはなにかをたべながらえいがをみるのがすきです。
watashi wa nanika o tabe nagara eiga o miru no ga suki desu.
I love to eat something while watching a movie.
 --- 
Example #7
電車とバスのどちらに乗るのが好きですか？
でんしゃとバスのどちらにのるのがすきですか？
densha to basu no dochira ni noru no ga suki desu ka?
Which do you like to ride more, trains or buses?
 --- 
Example #8
私の夫は私が夜に外出するのが好きではない。
わたしのおっとはわたしがよるにがいしゅつするのがすきではない。
watashi no otto wa watashi ga yoru ni gaishutsu suru no ga suki dewa nai.
My husband doesn't like me going out at night.
 --- 
Example #9
多くの日本人は多くの人の前で自分の意見を表現するのが好きじゃない。
おおくのにほんじんはおおくのひとのまえでじぶんのいけんをひょうげんするのがすきじゃない。
ooku no nihonjin wa ooku no hito no mae de jibun no iken o hyougen suru no ga suki janai.
Most Japanese people don't like expressing their opinions in front of a lot of people.