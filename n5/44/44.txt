Example #1
急がなくてはならない。
いそがなくてはならない。
isoga nakute wa naranai.
I have to hurry.
 --- 
Example #2
本当にすぐ行かなくてはならない。
ほんとうにすぐいかなくてはならない。
hontouni sugu ika nakute wa naranai.
I really have to go now.
 --- 
Example #3
もう10時だ。寝なくてはならない。
もう１０じだ。ねなくてはならない。
mou juu ji da. ne nakute wa naranai.
It's already 10 o'clock. I must go to sleep.
 --- 
Example #4
日本語をもっと勉強しなくてはいけない。
にほんごをもっとべんきょうしなくてはならない。
nihongo o motto benkyou shi nakute wa naranai.
I really need to study Japanese more.
 --- 
Example #5
花の水やりをしなくてはなりません。
はなのみずやりをしなくてはなりません。
hana no mizu yari o shi nakute wa narimasen.
You must water the flowers.
 --- 
Example #6
明日、早く起きなくてはなりません。
あした、はやくおきなくてはなりませんん。
ashita, hayaku oki nakute wa narimasen.
I have to get up early tomorrow.
 --- 
Example #7
野菜を食べなくてはなりませんよ。
やさいをたべなくてはなりませんよ。
yasai o tabe nakute wa narimasen yo.
You must eat your vegetables!
 --- 
Example #8
日本語能力試験を受けなくてはなりません。
にほんごのうりょくしけんをうけなくてはなりません。
nihongo nouryoku shiken o uke nakute wa narimasen.
I have to take the Japanese language proficiency test.
 --- 
Example #9
もう帰らなくてはなりません。
もうかえらなくてはなりません。
mou kaera nakute wa narimasen.
I must be heading back now.
 --- 
Example #10
日本語力を上達させなくてはなりません。
にほんごりょくをじょうたつさせなくてはなりません。
nihongoryoku o joutatsu sase nakute wa narimasen.
I have to improve my Japanese language abilities.