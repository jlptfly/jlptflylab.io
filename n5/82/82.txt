Example #1
毎朝、パンやベーコンなどを食べています。
まいあさ、パンやベーコンなどをたべています。
mai asa, pan ya beekon nado o tabeteimasu.
Every morning I eat bread and bacon and so on.
 --- 
Example #2
スーパーで肉や野菜を買いました。
スーパーでにくややさいをかいました。
suupaa de niku ya yasai o kaimashita.
I bought some meat and vegetables (and so on) from the supermarket.
 --- 
Example #3
あなたのおすすめの音楽や本を私に教えてください。
あなたのおすすめのおんがくやほんをわたしにおしえてください。
anata no osusume no ongaku ya hon o watashi ni oshiete kudasai.
Please tell me your recommended music or books.
 --- 
Example #4
電車やバスでの旅は好きです。
でんしゃやバスでのたびはすきです。
densha ya basu de no tabi wa suki desu.
I like traveling by train and bus (and so on).
 --- 
Example #5
私はアメリカやイギリスのテレビドラマを見るのが好きです。
わたしはアメリカやイギリスのテレビドラマをみるのがすきです。
watashi wa amerika ya igirisu no terebi dorama o miru no ga suki desu.
I like watching American and British dramas (among others).
 --- 
Example #6
冬休みは家族で長野や北海道へ行きます。
ふゆやすみはかぞくでながのやほっかいどうへいきます。
fuyu yasumi wa kazoku de nagano ya hokkaidou e ikimasu.
During winter vacation my family often goes to places like Nagano or Hokkaido.
 --- 
Example #7
私は数学や化学などが好きです。
わたしはすうがくやかがくなどがすきです。
watashi wa suugaku ya kagaku nado ga suki desu.
I like subjects like math, chemistry and so on.
 --- 
Example #8
秋には葉が赤や黄色に変わる。
あきにははがあかやきいろにかわる。
aki niwa ha ga aka ya kiiro ni kawaru.
The leaves turn red and yellow in fall.