Example #1
もっとお金が欲しいです。
もっとおかねがほしいです。
motto okane ga hoshii desu.
I want more money.
 --- 
Example #2
彼女がほしい。
かのじょがほしい。
kanojo ga hoshii.
I want a girlfriend.
 --- 
Example #3
お金がたくさんほしい。
おかねがたくさんほしい。
okane ga takusan hoshii.
I want a lot of money. (it's OK to split が and ほしい)
 --- 
Example #4
何人子どもがほしいですか？
なんにんこどもがほしいですか？
nan nin kodomo ga hoshii desu ka?
How many children do you want to have?
 --- 
Example #5
新しいGoProカメラが欲しいです！
あたらしいGoProカメラがほしいです！
atarashii gopuro kamera ga hoshii desu!
I want the new GoPro camera!
 --- 
Example #6
いつか自分の家が欲しいです。
いつかじぶんのいえがほしいです。
itsuka jibun no ie ga hoshii desu.
I want to have my own house one day.
 --- 
Example #7
私が欲しいものは自由です。
わたしがほしいものはじゆうです。
watashi ga hoshii mono wa jiyuu desu.
What I want is freedom.
 --- 
Example #8
日本で作られた車が欲しい。
にほんでつくられたくるまがほしい。
nihon de tsukurareta kuruma ga hoshii.
I want a car made in Japan.
 --- 
Example #9
最近、仕事が忙しすぎてリラックスする時間が欲しい。
さいきん、しごとがいそがしすぎてリラックスするじかんがほしい。
saikin, shigoto ga isogashi sugite rirakkusu suru jikan ga hoshii.
Work is too busy lately, I want some time to relax.
 --- 
Example #10
彼が持っているみたいなパソコンが欲しい。
かれがもっているみたいなパソコンがほしい。
kare ga motteiru mitai na pasokon ga hoshii.
I want a computer like the one he has.