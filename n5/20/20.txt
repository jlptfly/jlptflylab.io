Example #1
あなたは一人じゃない。
あなたはひとりじゃない。
anata wa hitori janai.
You are not alone.
 --- 
Example #2
危険じゃないの？
きけんじゃないの？
kiken janai no?
Isn't it dangerous?
 --- 
Example #3
そんなつもりじゃなかった。
そんなつもりじゃなかった。
sonna tsumori ja nakatta.
That wasn't my plan/intention.
 --- 
Example #4
ごめんなさい！わざとではありませんでした。
ごめんなさい！わざとではありませんでした。
gomennasai! wazato dewa arimasen deshita.
I'm so sorry! It wasn't intentional.
 --- 
Example #5
肉はあまり好きじゃないです。
にくはあまりすきじゃないです。
niku wa amari suki janai desu.
I don't really like meat.
 --- 
Example #6
若いころはやさいが好きじゃなかった。
わかいころはやさいがすきじゃなかった。
wakai koro wa yasai ga suki janakatta.
I didn't like vegetables when I was younger.
 --- 
Example #7
私は日本語が上手ではない。
わたしはにほんごがじょうずではない。
watashi wa nihongo ga jouzu dewa nai.
I am not good at Japanese (language).