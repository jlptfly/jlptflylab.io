Example #1
あの人は誰でしょう？
あのひとはだれでしょう？
ano hito wa dare deshou?
I wonder who that person is..
 --- 
Example #2
いいでしょう。
いいでしょう。
ii deshou.
That sounds/seems good.
 --- 
Example #3
この問題は簡単でしょう？
このもんだいはかんたんでしょう。
kono mondai wa kantan deshou?
This problem is easy, right?
 --- 
Example #4
彼はもうすぐ来るでしょう。
かれはもうすぐくるでしょう。
kare wa mou sugu kuru deshou.
He should be here any second.
 --- 
Example #5
頑張れば、いい大学に行けるでしょう。
がんばれば、いいだいがくにいけるでしょう。
ganbareba, ii daigaku ni ikeru deshou.
If you work hard, you should be able to get into a good university.
 --- 
Example #6
今日の試合では、Aチームが勝つでしょう。
きょうのしあいでは、Aチームがかつでしょう。
kyou no shiai de wa, A chiimu ga katsu deshou.
In today's match, surely team A will win.
 --- 
Example #7
この問題は、明日したの試験に出るでしょうか。
このもんだいは、あしたのしけんにでるでしょうか。
kono mondai wa, ashita no shiken ni deru deshou ka.
Will this question be on tomorrow's test?