Example #1
彼はおはしを使うのが下手だ。
かれはおはしをつかうのがへただ。
kare wa ohashi o tsukau no ga heta da.
He is bad at using chopsticks.
 --- 
Example #2
母は料理をするのが下手だ。
はははりょうりをするのがへただ。
haha wa ryouri o suru no ga heta da.
My mother is bad at cooking.
 --- 
Example #3
私は人としゃべるのが下手だ。
わたしは人としゃべるのがへただ。
watashi wa hito to shaberu no ga heta da.
I'm not good at talking with people.
 --- 
Example #4
かんじを勉強するのが下手です。
かんじをべんきょうするのがへたです。
kanji o benkyou suru no ga heta desu.
I'm not good at studying kanji.
 --- 
Example #5
日本語の字を書くのが下手だ。
にほんごのじをかくのがへただ。
nihongo no ji o kaku no ga heta da.
I'm not good at writing Japanese characters.
 --- 
Example #6
姉は歌うのが下手だと思います。
あねはうたうのがへただとおもいます。
ane wa utau no ga heta da to omoimasu.
I think my older sister is bad at singing.
 --- 
Example #7
トライアスロンをやってみたいですが泳ぐのが下手だ。
トライアスロンをやってみたいですがおよぐのがへただ。
toraiasuron o yatte mitai desuga oyogu no ga heta da.
I want to try doing a triathlon, but I'm no good at swimming.