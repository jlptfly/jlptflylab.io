Example #1
たくさんの人がいるなあ。
たくさんのひとがいるなあ。
takusan no hito ga iru naa.
Wow, there's a lot of people here.
 --- 
Example #2
これ、美味しいなあ。
これ、おいしいなあ。
kore, oishii naa.
This is really delicious.
 --- 
Example #3
暑くなってきたなあ。
あつくなってきたなあ。
atsu ku natte kita naa.
It's starting to get hotter.
 --- 
Example #4
それは本当かな。
それはほんとうかな。
sore wa hontou kana.
I wonder if that's true...
 --- 
Example #5
明日は晴れるかなあ。
あしたははれるかなあ。
ashita wa hareru kanaa.
I wonder if the weather will clear up tomorrow...
 --- 
Example #6
トイレはどこかな。
トイレはどこかな。
toire wa doko kana.
I wonder where the restroom is...
 --- 
Example #7
頭が痛いな。
あたまがいたいな。
atama ga itai na.
I've got a headache...
 --- 
Example #8
新し自転車が欲しいな～。
あたらしいじてんしゃがほしいなあ。
atarashii jitensha ga hoshii naa.
I really want a new bicycle...
 --- 
Example #9
あなたは全部一人で作りましたか？すごいなあ！
あなたはぜんぶひとりでつくりましたか？すごいなあ！
anata wa zenbu hitori de tsukuri mashita ka? Sugoi naa!
You made all of this by yourself? That's too awesome!