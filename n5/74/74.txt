Example #1
トイレに行ってもいいですか？
トイレにいってもいいですか？
toire ni ittemo ii desu ka?
May I go to the restroom?
 --- 
Example #2
うん、行ってもいいですよ！
うん、いってもいいですよ！
un, ittemo ii desu yo!
Sure, go a head!
 --- 
Example #3
それを食べてもいいですか。
それをたべてもいいですか。
sore o tabetemo ii desu ka?
Can I eat that?
 --- 
Example #4
私も一緒に行っていい？
わたしもいっしょにいっていい？
watashi mo isshoni itte ii?
Can I go with you?
 --- 
Example #5
ここに座ってもいいですか？
ここにすわってもいいですか？
koko ni suwattemo ii desu ka?
Is it OK if I sit here?
 --- 
Example #6
一つ質問を聞いていいですか？
ひとつしつもんをきいていいですか？
hitotsu shitsumon o kiite ii desu ka?
Do you mind if I ask you a question?
 --- 
Example #7
あなたと一緒に行ってもいいですよ。
あなたといっしょにいってもいいですよ。
anata to isshoni ittemo ii desu yo.
I don't mind going with you.
 --- 
Example #8
今話してもいいですか。
いまはなしてもいいですか。
ima hanashi temo ii desu ka?
Can you talk now?
 --- 
Example #9
わざわざ家に来なくてもいいですよ。
わざわざいえにこなくてもいいですよ。
wazawaza ie ni konaku temo ii desu yo.
You don't need to bother coming all the way to my home.