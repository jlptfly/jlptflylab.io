Example #1
何ですか？
なんですか？
nan desu ka?
What?
 --- 
Example #2
すみません、あなたも学生ですか。
すみません、あなたもがくせいですか。
sumimasen, anata mo gakusei desu ka.
Excuse me, are you also a student?
 --- 
Example #3
あなたの名前は何ですか。
あなたのなまえはなんですか。
anata no namae wa nandesu ka.
What is your name?
 --- 
Example #4
元気ですか。
げんきですか。
genki desu ka.
How are you?
 --- 
Example #5
彼は何才ですか。
かれはなんさいですか。
kare wa nansai desu ka.
How old is he?
 --- 
Example #6
大丈夫ですか？
だいじょうぶですか？
daijoubu desu ka?
Are you alright?
 --- 
Example #7
私とお昼ご飯を食べませんか？
わたしとおひるごはんをたべませんか？
watashi to ohiru gohan wo tabemasen ka?
Would you like to have lunch with me?
 --- 
Example #8
どうしてこのことを知らないのか？
どうしてこのことをしらないのか？
doushite kono koto wo shiranai no ka?
Why don't you know about this?