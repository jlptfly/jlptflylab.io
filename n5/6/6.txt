Example #1
私は魚が好すきです。でも肉も好きです。
わたしはさかながすきです。でもにくもすきです。
watashi wa sakana ga suki desu. demo niku mo suki desu.
I like fish. But I also like meat too.
 --- 
Example #2
パン屋に行きました。でも、何も買いませんでした。
パンやにいきました。でも、なにもかいませんでした。
panya ni ikimashita. demo, nanimo kaimasen deshita.
I went to the bakery, but didn't buy anything.
 --- 
Example #3
動物が好きです。でも、犬が一ばん好きです。
どうぶつがすきです。でも、いぬがいちばんすきです。
doubutsu ga suki desu. demo, inu ga ichiban suki desu.
I like animals. But I love dogs the most.
 --- 
Example #4
図書館に行きました。でも、集中できませんでした。
としょかんにいきました。でも、しゅうちゅうできませんでした。
toshokan ni ikimashita. demo, shuuchuu dekimasen deshita.
I went to the library. But I wasn't able to concentrate.
 --- 
Example #5
スーパーに行きました。でも、財布を忘れました！
スーパーにいきました。でも、さいふをわすれました！
suupaa ni ikimashita. demo, saifu wo wasuremashita.
I went to the grocery store. But I forgot my wallet!
 --- 
Example #6
今朝学校に行きました。でも、休みでした。
けさがっこうにいきました。でも、やすみでした。
kesa gakkou ni ikimashita. demo, yasumi deshita.
I went to school this morning. but it was closed.
 --- 
Example #7
日本人ともっと話したいです。でも、私の日本語はあまり上手ではありません。
にほんじんともっとはなしたいです。でも、わたしのにほんごはあまりじょうずではありません。
nihonjin to motto hanashitai desu. demo, watashi no nihongo wa amari jouzu dewa arimasen.
I want to speak to Japanese people more. But my Japanese is not so good.