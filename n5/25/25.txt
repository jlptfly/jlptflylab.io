Example #1
仕事は辛いけど楽しい。
しごとはつらいけどたのしい。
shigoto wa tsurai kedo tanoshii.
Work is tough, but fun.
 --- 
Example #2
金はないけど夢はある。
かねはないけどゆめはある。
kane wa nai kedo yume wa aru.
I don't have money, but I have dreams.
 --- 
Example #3
スポーツは上手じゃないけど、好きです。
スポーツはじょうずじゃないけど、すきです。
supootsu wa jouzu janai kedo, suki desu.
I'm not good at sports, but I like them.
 --- 
Example #4
悪いけど、明日のパーティーに行けません。
わるいけど、あしたのパーティーにいけません。
warui kedo, ashita no paatii ni ikemasen.
Sorry, but I can't make it to tomorrow's party.
 --- 
Example #5
眠いけど、まだ宿題があるから寝られません。
ねむいけど、まだしゅくだいがあるからねられません。
nemui kedo, mada shukudai ga aru kara neraremasen.
I'm sleepy, but I still have homework to do so I can't go to sleep yet.
 --- 
Example #6
たくさん勉強したけど、テストの点はよくないでした。
たくさんべんきょうしたけど、テストのてんはよくないでした。
takusan benkyou shita kedo, tesuto no ten wa yokunai deshita.
Although I studied a lot, my test score wasn't very good.
 --- 
Example #7
泣いたけど後悔はしていません。
ないたけどこうかいはしていません。
naita kedo koukai wa shiteimasen.
I cried, but I don't regret it.
 --- 
Example #8
家賃は高いけどこのマンションが大好きです。
やちんはたかいけどこのマンションがだいすきです。
yachin wa takai kedo kono manshon ga daisuki desu.
The rent is a bit expensive, but I love this apartment.