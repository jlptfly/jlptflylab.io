Example #1
明日は雨が降る。
あしたはあめがふる。
ashita wa ame ga furu.
It is going to rain tomorrow.
 --- 
Example #2
仕方がない。
しかたがない。
shikata ga nai.
It cannot be helped. (a common expression in Japanese)
 --- 
Example #3
コンビニが近くにあります。
コンビニがちかくにあります。
konbini ga chikaku ni arimasu.
There is a convenience store nearby.
 --- 
Example #4
彼は借金がある。
かれはしゃっきんがある。
kare wa shakkin ga aru.
He is in debt.
 --- 
Example #5
今日は、やることがたくさんある。
きょうは、やることがたくさんある。
kyou wa, yaru koto ga takusan aru.
There are a lot of things to do today.
 --- 
Example #6
彼は金はあるが、バカな男だ。
かれはかねはあるが、ばかなおとこだ。
kare wa kane wa aru ga, baka na otoko da.
He has money, but he's a stupid man.
 --- 
Example #7
私は日本語を話すことはできますが、読むことはできません。
わたしはにほんごをはなすことはできますが、よむことはできません。
watashi wa nihongo o hanasu koto wa dekimasu ga, yomu koto wa dekimasen.
I can speak Japanese, but I am not able to read it.
 --- 
Example #8
このカメラを買いたいですがお金がない。
このカメラをかいたいですがおかねがない。
kono kamera o kaitai desu ga okane ga nai.
I want to buy this camera, but I have no money..
 --- 
Example #9
恐れ入りますが、本日は満席です。
おそれいれますが、ほんじつはまんせきです。
osore irimasu ga, honjitsu wa manseki desu.
I deeply apologize, but today we have no more seats available. (we are fully booked)